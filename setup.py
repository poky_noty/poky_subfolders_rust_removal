from setuptools import setup

setup( 
    name='subs_wizard',
    version='2.8.3',
    description='Fight Back the Subfolders Fever',
    long_description='Not given yet !',
    author='Miltos Vimplis',
    url="https://gitlab.com/poky_noty/poky_subfolders_rust_removal",
    license='MIT License',
    packages=['poky_noty'],
    #package_data=[],
    entry_points={
        'console_scripts': [
            'subs_wizard_flat=poky_noty.test_flat:main',
            'subsW_one_subf=poky_noty.dummyF_A.test_one_subf:main',
            'subsW_two_subf=poky_noty.dummyF_B.test_two_subf:main',
        ]
    }, 
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Programming Language :: Python :: 3.4',
        'Topic :: System :: Linux :: Embedded',
    ],
)
